# EMC数据包  

#### 介绍
这是一个运行在 Minecraft EMC 服务器上的[数据包](https://gitee.com/a1_panda/EMC-datapack/releases) 
#### 内容 
这将会修改游戏的 **配方，进度，战利品，标签** 以及一些原版特性。
可以下载该数据包丢入[.minecraft\saves\你的存档\datapacks]即可载入数据包
